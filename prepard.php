<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "demo_1";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// prepare and bind 
$stmt = $conn->prepare("INSERT INTO details(name_,address_) VALUES (?,?)");
$stmt->bind_param("ss", $firstname, $lastname);

// set parameters and execute
$firstname = "sanket";
$lastname = "austeliya";

$stmt->execute();

$firstname = "Spider";
$lastname = "Mexico";
$stmt->execute();

$firstname = "karan";
$lastname = "kolkata";
$stmt->execute();

echo "New records created successfully";

$stmt->close();
$conn->close();
?> 